﻿using AutoMapper;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Test.Models;
using Test.Repositories;
using Test.Utilities;
using Test.ViewModels;

namespace Test.Controllers.Api
{
    [RoutePrefix("api/upload")]
    public class FileUploadController : ApiController
    {
        private IFileUploadRepository fileUploadRepository;
        private IInvalidTransactionRepository invalidTransactionRepository;

        public FileUploadController(IFileUploadRepository fileUploadRepository, IInvalidTransactionRepository invalidTransactionRepository)
        {
            this.fileUploadRepository = fileUploadRepository;
            this.invalidTransactionRepository = invalidTransactionRepository;
        }

        [Route()]
        public async Task<int> PostFormData()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            FileUploadDetailViewModel fileUploadDetailViewModel = new FileUploadDetailViewModel();

            CustomFormDataStreamProvider provider = await fileUploadRepository.SaveFileToServer(Request);

            int fileUploadId = 0;
            if (provider.FileData.Any())
            {
                // only load first file
                fileUploadId = fileUploadRepository.LoadToDatabase(provider.FileData[0].LocalFileName);
            }

            return fileUploadId;
        }

        [Route("")]
        [Route("{fileUploadId}")]
        public FileUploadDetailViewModel Get(int fileUploadId)
        {
            FileUpload fileUpload = this.fileUploadRepository.GetById(fileUploadId);
            return Mapper.Map<FileUploadDetailViewModel>(fileUpload);
        }

        [Route("")]
        public IEnumerable<FileUploadViewModel> Get()
        {
            return Mapper.Map<IEnumerable<FileUploadViewModel>>(fileUploadRepository.Get());
        }
    }
}
