﻿using AutoMapper;
using System.Collections.Generic;
using System.Web.Http;
using Test.Models;
using Test.Repositories;
using Test.ViewModels;

namespace Test.Controllers.Api
{
    [RoutePrefix("api/transaction")]
    public class TransactionController : ApiController
    {
        private ITransactionRepository transactionRepository;

        public TransactionController(ITransactionRepository transactionRepository)
        {
            this.transactionRepository = transactionRepository;
        }

        [Route("")]
        public Paged<TransactionViewModel> Get(int page = 0, int recordsToReturn = 0)
        {
            int totalRecords = 0;
            int startRecord = page * recordsToReturn;

            IEnumerable<Transaction> transactions = transactionRepository.Get(out totalRecords, startRecord, recordsToReturn);
            IEnumerable<TransactionViewModel> models = Mapper.Map<IEnumerable<TransactionViewModel>>(transactions);
            Paged<TransactionViewModel> pagedData = new Paged<TransactionViewModel>(models, totalRecords);
            return pagedData;
        }

        [Route("")]
        [Route("{transactionId}")]
        public TransactionViewModel Get(int transactionId)
        {
            Transaction fileUpload = this.transactionRepository.GetById(transactionId);
            return Mapper.Map<TransactionViewModel>(fileUpload);
        }

        [Route("")]
        public void Post(TransactionViewModel model)
        {
            Transaction transaction = Mapper.Map<Transaction>(model);
            this.transactionRepository.Save(transaction);
        }

        [Route("")]
        [Route("{transactionId}")]
        public void Delete(int transactionId)
        {
            this.transactionRepository.Delete(transactionId);
        }
    }
}
