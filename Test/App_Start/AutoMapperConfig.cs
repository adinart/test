﻿using AutoMapper;
using Test.Models;
using Test.ViewModels;

namespace Test
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<FileUpload, FileUploadViewModel>();
                cfg.CreateMap<InvalidTransaction, InvalidTransactionViewModel>();
                cfg.CreateMap<FileUpload, FileUploadDetailViewModel>();
                cfg.CreateMap<Transaction, TransactionViewModel>();
            });

            Mapper.AssertConfigurationIsValid();
        }
    }
}