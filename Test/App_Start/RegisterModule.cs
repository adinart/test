﻿using Autofac;
using System.Net.Http;
using System.Web.Configuration;
using Test.DAL;
using Test.Repositories;
using Test.Utilities;

namespace Test
{
    public class RegisterModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register<TestDBContext>(t => { return new TestDBContext(); });

            builder.RegisterType<FileUploadRepository>().As<IFileUploadRepository>();
            builder.RegisterType<InvalidTransactionRepository>().As<IInvalidTransactionRepository>();
            builder.RegisterType<TransactionRepository>().As<ITransactionRepository>();

            builder.Register<CustomFormDataStreamProvider>(t =>
            {
                string path = WebConfigurationManager.AppSettings["UploadFilePath"];
                return new CustomFormDataStreamProvider(path);
            });
        }
    }
}