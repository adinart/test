﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.ViewModels
{
    public class FileUploadViewModel
    {
        public int FileUploadId { get; set; }
        public string FileName { get; set; }
        public DateTime UploadDate { get; set; }
        public int UploadedRow { get; set; }
    }
}