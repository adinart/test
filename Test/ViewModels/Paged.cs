﻿using System.Collections.Generic;

namespace Test.ViewModels
{
    public class Paged<T> where T : class
    {
        public Paged(IEnumerable<T> data, int totalRecords)
        {
            this.Data = data;
            this.TotalRecords = totalRecords;
        }

        public IEnumerable<T> Data { get; set; }
        public int TotalRecords { get; set; }
    }
}