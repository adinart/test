﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.ViewModels
{
    public class InvalidTransactionViewModel
    {
        public string Detail { get; set; }
        public string Message { get; set; }
    }
}