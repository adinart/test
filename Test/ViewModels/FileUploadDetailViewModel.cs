﻿using System;
using System.Collections.Generic;

namespace Test.ViewModels
{
    public class FileUploadDetailViewModel
    {
        public int FileUploadId { get; set; }
        public string FileName { get; set; }
        public DateTime UploadDate { get; set; }
        public int UploadedRow { get; set; }
        public IEnumerable<InvalidTransactionViewModel> InvalidTransactions { get; set; }
    }
}