﻿using Test.DAL;
using Test.Models;

namespace Test.Repositories
{
    public class InvalidTransactionRepository : BaseRepository<InvalidTransaction>, IInvalidTransactionRepository
    {
        public InvalidTransactionRepository(TestDBContext context)
            : base(context)
        {
        }

        public override void Save(InvalidTransaction item)
        {
            throw new System.NotImplementedException();
        }
    }
}