﻿using System.Collections.Generic;
namespace Test.Repositories
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Get();
        TEntity GetById(int id);
        void Save(TEntity item);
        bool TryDelete(int id);
        void Commit();
    }
}
