﻿using System.Net.Http;
using System.Threading.Tasks;
using Test.Models;
using Test.Utilities;
using Test.ViewModels;

namespace Test.Repositories
{
    public interface IFileUploadRepository: IBaseRepository<FileUpload>
    {
        Task<CustomFormDataStreamProvider> SaveFileToServer(HttpRequestMessage request);
        int LoadToDatabase(string fileName);
    }
}