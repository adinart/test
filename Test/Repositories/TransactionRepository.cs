﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Test.DAL;
using Test.Models;

namespace Test.Repositories
{
    public class TransactionRepository : BaseRepository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(TestDBContext context)
            : base(context)
        {
        }

        public IEnumerable<Transaction> Get(out int totalRecords, int startRecord = 0, int recordsToReturn = 0)
        {
            var transactions = base.Get().Skip(startRecord).Take(recordsToReturn);
            totalRecords = base.Get().Count();
            return transactions;
        }

        public override void Save(Transaction item)
        {
            if (item.TransactionId > 0)
            {
                var originalItem = this.dbSet.AsNoTracking().SingleOrDefault(x => x.TransactionId == item.TransactionId);
                if (originalItem != null)
                {
                    this.dbSet.Attach(item);
                    this.context.Entry<Transaction>(item).State = EntityState.Modified;
                }
            }
            else
            {
                this.dbSet.Add(item);
            }

            this.context.SaveChanges();
        }

        public void Delete(int transactionId)
        {
            if (base.TryDelete(transactionId))
            {
                base.Commit();
            }
        }
    }
}