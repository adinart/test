﻿using System.Collections.Generic;
using System.Data.Entity;
using Test.DAL;

namespace Test.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        internal TestDBContext context;
        internal DbSet<TEntity> dbSet;

        public BaseRepository(TestDBContext context)
        {
            this.context = context;
            this.dbSet = this.context.Set<TEntity>();
        }

        public IEnumerable<TEntity> Get()
        {
            return this.dbSet;
        }

        public virtual TEntity GetById(int id)
        {
            return this.dbSet.Find(id);
        }

        public abstract void Save(TEntity item);

        public bool TryDelete(int id)
        {
            TEntity item = this.dbSet.Find(id);
            if (item == null)
                return false;
            else
            {
                this.dbSet.Remove(item);
                return true;
            }
        }

        public void Add(TEntity item)
        {
            this.dbSet.Add(item);
        }

        public void Edit(TEntity item)
        {
            context.Entry(item).State = EntityState.Modified;
        }

        public void Commit()
        {
            this.context.SaveChanges();
        }
    }
}