﻿using System.Collections.Generic;
using Test.Models;

namespace Test.Repositories
{
    public interface ITransactionRepository : IBaseRepository<Transaction>
    {
        IEnumerable<Transaction> Get(out int totalRecords, int startRecord = 0, int recordsToReturn = 0);
        void Delete(int categoryId);
    }
}