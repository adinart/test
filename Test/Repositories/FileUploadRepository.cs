﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using Test.DAL;
using Test.Models;
using Test.Utilities;

namespace Test.Repositories
{
    public class FileUploadRepository : BaseRepository<FileUpload>, IFileUploadRepository
    {
        CustomFormDataStreamProvider provider;

        public FileUploadRepository(TestDBContext context, CustomFormDataStreamProvider provider)
            : base(context)
        {
            this.provider = provider;
        }

        public override void Save(FileUpload item)
        {
            throw new System.NotImplementedException();
        }

        public async Task<CustomFormDataStreamProvider> SaveFileToServer(HttpRequestMessage request)
        {
            return await request.Content.ReadAsMultipartAsync(provider);
        }

        public int LoadToDatabase(string fileName)
        {
            SqlParameter param1 = new SqlParameter("@file", fileName);
            SqlParameter param2 = new SqlParameter() { ParameterName = "@fileUploadId", DbType = System.Data.DbType.Int16, Direction = System.Data.ParameterDirection.Output };
            context.Database.ExecuteSqlCommand("BulkInsertTransaction @file, @fileUploadId out", param1, param2);

            int fileUploadId = 0;
            if (param2.Value != null) 
            {
                fileUploadId = Convert.ToInt32(param2.Value); 
            }

            return fileUploadId;
        }         
    }
}