﻿
using Test.Models;

namespace Test.Repositories
{
    public interface IInvalidTransactionRepository : IBaseRepository<InvalidTransaction>
    {
    }
}
