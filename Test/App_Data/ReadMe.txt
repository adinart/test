﻿1.	Create database 
2.	Run database scripts in App_Data folder in the following order: 01-tables.sql, 02-storedprocedures.sql and 03-data.sql
3.	Configure connectionString in web.config to point to the database
6.	Configure appSetting key UploadFilePath to point to a location where the uploaded file will be saved
7.	If it is not exist, create directory in appSetting key UploadFilePath 
8.	Click F5 to run the solution locally, the application should automatically display the Upload File page

Note:
1. You can only load csv files. The application presumes CSV files first row to be column headers.
2. The main page is the Upload File page - http://localhost:xxxx/fileupload
   Select a file and click Upload button to load a file. Detail of the result of file upload will be displayed after each upload execution.
3. The Upload File page also lists previous file uploads. By clicking Detail, you can view failed rows for each upload.  
4. To view uploaded transactions, go to Transaction page by clicking the Transaction menu on the navigation bar.
   The url will be http://localhost:xxxx/transaction.
5. The file ~/App_Data/test1.csv was used for testing. 
   It contains 100,000 rows. Upon upload, 99,991 rows loaded successfully and 9 rows failed to load.