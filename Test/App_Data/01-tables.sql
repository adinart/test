﻿
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Currency]') AND type in (N'U'))
DROP TABLE [dbo].[Currency]
GO

CREATE TABLE Currency (
	Code nvarchar(3) not null,
	CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED(Code ASC),
)
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transaction]') AND type in (N'U'))
DROP TABLE [dbo].[Transaction]
GO

CREATE TABLE [Transaction]  (
 TransactionId int identity(1,1) not null,
 Account nvarchar (256) not null,
 [Description] nvarchar(max),
 CurrencyCode nvarchar(3),
 Amount decimal (18,2) 
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED(TransactionId ASC),
 CONSTRAINT [FK_Transaction_Currency] FOREIGN KEY (CurrencyCode) References Currency(Code)
)
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileUpload]') AND type in (N'U'))
DROP TABLE [dbo].[FileUpload]
GO

CREATE TABLE FileUpload (
	FileUploadId int identity(1,1) not null,
	[FileName] nvarchar(50) not null,
	UploadDate datetime not null default(getdate()),
	UploadedRow int not null default(0),
	CONSTRAINT [PK_FileUpload] PRIMARY KEY CLUSTERED(FileUploadId ASC),
)

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InvalidTransaction]') AND type in (N'U'))
DROP TABLE [dbo].[InvalidTransaction]
GO

CREATE TABLE [InvalidTransaction] (
 InvalidTransactionId int identity(1,1) not null,
 FileUploadId int not null, -- fk to fileupload table
 [Detail] nvarchar(max)not null,
 [Message] nvarchar(max) not null,
 CONSTRAINT [FK_InvalidTransaction_FileUpload] FOREIGN KEY (FileUploadId) References FileUpload(FileUploadId)
)
GO