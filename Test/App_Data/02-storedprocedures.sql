﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BulkInsertTransaction]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[BulkInsertTransaction]
GO

CREATE PROC [dbo].[BulkInsertTransaction] 
(
	@file nvarchar(500),
	@fileUploadId int OUT
)
AS
BEGIN
	insert into FileUpload ([FileName]) values (@file)
	set @fileUploadId = SCOPE_IDENTITY()

	IF EXISTS (SELECT * FROM  sys.objects WHERE name  = 'TransactionTemp' and TYPE =  'U')
		DROP TABLE  TransactionTemp

	CREATE TABLE [TransactionTemp] (
	 Account nvarchar (256) ,
	 [Description] nvarchar(max),
	 CurrencyCode nvarchar(3),
	 Amount nvarchar(25)
	)

	declare @insert nvarchar(max) 

	set @insert =
		'BULK INSERT [TransactionTemp]
			FROM ''' + @file + '''
			WITH
			(
			FIRSTROW = 2,
			FIELDTERMINATOR = '','',  --CSV field delimiter
			ROWTERMINATOR = ''\n'',   --Use to shift the control to next row
			TABLOCK
			)'			
			
	exec (@insert)
	
	ALTER TABLE [TransactionTemp] ADD Id int identity(1,1)
	ALTER TABLE [TransactionTemp] ADD CONSTRAINT [PK_TransactionTemp] PRIMARY KEY CLUSTERED(Id)
	
	declare @i int, @count int, @account nvarchar(256), 
	@description nvarchar(max), @currency nvarchar(3), @amount nvarchar(25), @message nvarchar(max)
	set @i = 1
	select @count = COUNT(*) from [TransactionTemp]
	
	while (@i < @count+1)
	begin	
		set @message = ''
		select @account = Account, @description = [Description], @currency = CurrencyCode, @amount = Amount
		from [TransactionTemp] where Id = @i
		
		if (ISNULL(@account,'') = '')
			set @message = 'Account is empty'
		if (ISNULL(@description,'') = '')
		begin
			if (@message = '')
				set @message = 'Description is empty'
			else
				set @message = @message + '; Description is empty'
		end
		if (ISNULL(@currency,'') = '')
		begin
			if (@message = '')
				set @message = 'Currency Code is empty'
			else
				set @message = @message + '; Currency Code is empty'
		end
		if not exists(select top 1 * from Currency where Code = @currency)
		begin
			if (@message = '')
				set @message = 'Currency Code is invalid'
			else
				set @message = @message + '; Currency Code is invalid'
		end
		if (ISNULL(@amount,'') = '')
		begin
			if (@message = '')
				set @message = 'Amount is empty'
			else
				set @message = @message + '; Amount is empty'
		end
		if (ISNUMERIC(@amount) != 1)
		begin
			if (@message = '')
				set @message = 'Amount is invalid'
			else
				set @message = @message + '; Amount is invalid'
		end
		if (@message != '')
		begin
			declare @detail nvarchar(MAX)
			set @detail = 'Row ' + str(@i) + ' - Account: ' + ISNULL(@account,'') + ', Description: ' +  ISNULL(@description,'') + ', Currency Code: ' + ISNULL(@currency,'') + ', Amount: ' + ISNULL(@amount,'')
		
			insert into [InvalidTransaction] (FileUploadId, Detail, [Message])
			values (@fileUploadId, @detail, @message)
			
			delete [TransactionTemp] where Id = @i	
		end		
		
		set @i = @i + 1	
	end
	
	insert into [Transaction] (Account,Description,CurrencyCode,Amount)
	select Account,Description,CurrencyCode,Amount from [TransactionTemp] 
	
	update FileUpload set UploadedRow = @@ROWCOUNT where FileUploadId = @fileUploadId 
END

GO