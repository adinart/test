﻿using System.Data.Entity;
using Test.Models;

namespace Test.DAL
{
    public interface IUnitOfWork
    {
        IDbSet<Currency> Currencies { get; }
        IDbSet<Transaction> Transactions { get; }
        IDbSet<FileUpload> FileUploads { get; }
    }
}
