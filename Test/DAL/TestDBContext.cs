﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Test.Models;

namespace Test.DAL
{
    public class TestDBContext : DbContext
    {
        public TestDBContext()
            : base("TestDB")
        {
            Database.SetInitializer<TestDBContext>(null);
        }

        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<FileUpload> FileUploads { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Currency>().HasKey(c => c.Code);

            modelBuilder.Entity<Transaction>()
                .HasRequired(t => t.Currency)
                .WithMany(c => c.Transactions)
                .HasForeignKey(t => t.CurrencyCode);
        }
    }
}