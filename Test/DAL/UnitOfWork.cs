﻿using System.Data.Entity;
using Test.Models;

namespace Test.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private TestDBContext db;

        public UnitOfWork(TestDBContext db)
        {
            this.db = db;
        }

        public IDbSet<Currency> Currencies { get { return db.Currencies; } }
        public IDbSet<Transaction> Transactions { get { return db.Transactions; } }
        public IDbSet<FileUpload> FileUploads { get { return db.FileUploads; } }
    }
}