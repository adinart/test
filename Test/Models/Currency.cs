﻿using System.Collections.Generic;

namespace Test.Models
{
    public class Currency
    {
        public string Code { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}