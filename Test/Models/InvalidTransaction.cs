﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Models
{
    public class InvalidTransaction
    {
        public int InvalidTransactionId { get; set; }
        public int FileUploadId { get; set; }
        public string Detail { get; set; }
        public string Message { get; set; }

        public virtual FileUpload FileUpload { get; set; }
    }
}