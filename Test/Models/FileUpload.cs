﻿using System;
using System.Collections.Generic;

namespace Test.Models
{
    public class FileUpload
    {
        public int FileUploadId { get; set; }
        public string FileName { get; set; }
        public DateTime UploadDate { get; set; }
        public int UploadedRow { get; set; }

        public virtual ICollection<InvalidTransaction> InvalidTransactions { get; set; }
    }
}