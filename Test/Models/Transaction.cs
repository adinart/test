﻿namespace Test.Models
{
    public class Transaction
    {
        public int TransactionId { get; set; }
        public string Account { get; set; }
        public string Description { get; set; }
        public string CurrencyCode { get; set; }
        public decimal Amount { get; set; }

        public virtual Currency Currency { get; set; }
    }
}