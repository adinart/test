﻿'use strict';

var app = angular.module('testApp');

app.controller('FileUploadDetailController', ['$scope', '$http', '$route', '$routeParams', '$location', '$filter', 'FileUploadService',
    function ($scope, $http, $route, $routeParams, $location, $filter, FileUploadService) {

        $scope.pageTitle = "File Upload Detail";
        $scope.fileUploadId = $routeParams.fileUploadId;
        $scope.messages = [];

        $scope.upload = function () {
            var file = $scope.files[0];
            FileUploadService.upload(file);
        };

        FileUploadService.getById($scope.fileUploadId).then(function (data) {
            $scope.fileName = data.fileName;
            $scope.uploadDate = data.uploadDate;
            $scope.invalidTransactions = data.invalidTransactions;
            $scope.uploadedRow = data.uploadedRow;
        },
       function (error) {
           $scope.messages.push({ type: 'alert', message: error.status + ' - ' + error.statusText });
       });

        $scope.$watch('uploadDate', function (newValue) {
            $scope.uploadDate = $filter('date')(newValue, "dd/MM/yyyy 'at' h:mma");
        });
    }]);