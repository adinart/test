﻿'use strict';

var app = angular.module('testApp');

app.controller('TransactionController', ['$scope', '$route', '$routeParams', '$location', '$timeout', '$uibModal', 'TransactionService', function ($scope, $route, $routeParams, $location, $timeout, $uibModal, TransactionService) {

    $scope.pageTitle = "Transaction";
    $scope.messages = [];
    $scope.isLoading = false;

    $scope.pagerSettings = { currentPage: 1, pageSize: 1000, totalRows: 0, maxSize: 10, numberOfPages: 0, currentPageInputText: 1 };

    $scope.$watch('pagerSettings.currentPageInputText', function (newValue, oldValue) {
        $timeout(function () {
            $scope.pagerSettings.currentPage = $scope.pagerSettings.currentPageInputText;
        }, 1500);
    });

    $scope.$watch('pagerSettings.currentPage', function (newValue, oldValue) {
        if (newValue != oldValue) {
            load();
        }
    });

    // delete confirmation
    $scope.ConfirmDelete = function (transaction) {
        var dlg = $uibModal.open({
            templateUrl: 'confirmDeleteModal.html',
            controller: function ($scope, $uibModalInstance) {
                $scope.ok = function () {
                    $uibModalInstance.close(true);
                };

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            },
            windowClass: 'tiny'
        });

        dlg.result.then(function (btn) { // OK
            $scope.messages.length = 0;
            TransactionService.delete(transaction.transactionId)
                    .then(function () {
                        load();
                        $scope.messages.push({ type: 'alert-success', message: 'Transaction deleted' });
                    },
                    function (error) {
                        $scope.messages.push({ type: 'alert-danger', message: error.status + ' - ' + error.statusText });
                    });
        });
    }

    var load = function () {
        $scope.messages.length = 0;
        $scope.isLoading = true;
        TransactionService.get($scope.pagerSettings.currentPage - 1, $scope.pagerSettings.pageSize).then(
            function (result) {
                $scope.transactions = result.data;
                $scope.pagerSettings.totalRows = result.totalRecords;
                $scope.pagerSettings.currentPageInputText = $scope.pagerSettings.currentPage;
                $scope.isLoading = false;
        },
       function (error) {
           $scope.isLoading = false;
           $scope.messages.push({ type: 'alert', message: error.status + ' - ' + error.statusText });
       });
    }

    load();

}]);