﻿'use strict';

var app = angular.module('testApp');

app.controller('FileUploadController', ['$scope', '$http', '$route', '$routeParams', '$location', 'FileUploadService',
    function ($scope, $http, $route, $routeParams, $location, FileUploadService) {

    $scope.pageTitle = "File Upload";    
    $scope.messages = [];
    $scope.isLoading = false;

    $scope.upload = function () {
        $scope.messages.length = 0;
        if (!$scope.files || $scope.files.length == 0)
        {
            $scope.messages.push({ type: 'alert-danger', message: 'Please select a file' });
            return;
        }

        var file = $scope.files[0];
        if (file.name.split('.').pop() != 'csv') {
            $scope.messages.push({ type: 'alert-danger', message: 'Please use .csv file' });
            return;
        }
        
        $scope.isLoading = true;
 
        FileUploadService.upload(file).then(function (data) {
            $scope.isLoading = false;
            $location.path('/fileupload/detail/' + data);
        }, function (error) {
            $scope.isLoading = false;
            $scope.messages.push({ type: 'alert-danger', message: 'Fail upload fail: ' + error.status + ' - ' + error.statusText });
        });
       
    };
        
    var load = function () {
        $scope.messages.length = 0;
        FileUploadService.get().then(function (data) {
            $scope.fileUploads = data;
        },
       function (error) {
           $scope.messages.push({ type: 'alert-danger', message: error.status + ' - ' + error.statusText });
       });
    }

    load();
}]);