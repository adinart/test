﻿'use strict';

angular.module('testApp.services', []);
angular.module('testApp.directives', ['ui.bootstrap']);

var app = angular.module('testApp', ['ngRoute', 'testApp.services', 'testApp.directives']);

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $routeProvider
    .when('/', { templateUrl: '/template/fileupload/index', controller: 'FileUploadController', caseInsensitiveMatch: true })
    .when('/fileupload', { templateUrl: '/template/fileupload/index', controller: 'FileUploadController', caseInsensitiveMatch: true })
    .when('/fileupload/detail/:fileUploadId', { templateUrl: '/template/fileupload/detail', controller: 'FileUploadDetailController', caseInsensitiveMatch: true })
    .when('/transaction', { templateUrl: '/template/transaction/index', controller: 'TransactionController', caseInsensitiveMatch: true })
    .otherwise({ redirectTo: '/' });    
}]);

//app.run
