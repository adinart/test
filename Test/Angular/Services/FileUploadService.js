﻿'use strict';

var service = angular.module('testApp.services');

service.factory('FileUploadService', ['$http',
    function ($http) {
        return {
            upload: function (file) {
                var fd = new FormData();
                fd.append('file', file);
                var promise = $http.post('api/upload', fd, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                }).then(function (response) {
                    return response.data;
                });

                return promise;
            },
            get: function () {
                var promise = $http({
                    method: 'GET',
                    url: '/api/upload',
                }).then(function (response) {
                    return response.data;
                });

                return promise;
            },
            getById: function (fileUploadId) {
                var promise = $http({
                    method: 'GET',
                    url: '/api/upload/',
                    params: { fileUploadId: fileUploadId }
                }).then(function (response) {
                    return response.data;
                });

                return promise;
            }
        };
    }]);