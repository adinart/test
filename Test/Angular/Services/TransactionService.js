﻿'use strict';

var service = angular.module('testApp.services');

service.factory('TransactionService', ['$http',
        function ($http) {
            return {
                get: function (page, recordsToReturn) {
                    var promise = $http({
                        method: 'GET',
                        url: 'api/transaction',
                        params: { page: page, recordsToReturn: recordsToReturn }
                    }).then(function (response) { return response.data });

                    return promise;
                },
                edit: function (transaction) {
                    var promise = $http({
                        method: 'POST',
                        url: 'api/transaction',
                        params: { transaction: transaction }
                    }).then(function (response) { return response.data });

                    return promise;
                },
                delete: function (transactionId) {
                    var promise = $http({
                        method: 'DELETE',
                        url: '/api/transaction',
                        params: { transactionId: transactionId }
                    }).then(function (response) {
                        return response.data;
                    });

                    return promise;
                }
            };
    }]);