﻿'use strict';


var app = angular.module('testApp.directives');

app.directive('fileInput', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {            
            elm.bind('change', function () {
                scope.$apply(function() {
                    $parse(attrs.fileInput)
                    .assign(scope, elm[0].files);
                });
            });
        }
    }
}]);